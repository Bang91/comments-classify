package com.bang.crawler.synonmy;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.bang.thelion.emotion.RedisEmotionWordsManager;

public class EmotionWordExtender {
    private SynonymFetcher synonymFetcher = new SynonymFetcher();
    private RedisEmotionWordsManager emotionWordsManager = new RedisEmotionWordsManager();

    private RedisEmotionWordsManager sysnonymWordsManager = new RedisEmotionWordsManager("sysnonymwords");
    private String OVERLAP_VALUE = "overlap";
    private RedisEmotionWordsManager overlapWordsManager = new RedisEmotionWordsManager("overlapwords");
    
    public void addSynonym(String word, String emotion) throws IOException,
            InterruptedException {
        List<String> synonym = synonymFetcher.fetchBaidu(word);
        for (String w : synonym) {
            System.out.print(w + ": " + emotion + "\n");
            String existEmotion = sysnonymWordsManager.getCat(w);
            if (existEmotion == null)
                sysnonymWordsManager.add(w, emotion);
            else if (existEmotion != emotion) {
                sysnonymWordsManager.add(w, OVERLAP_VALUE);
                overlapWordsManager.add(w, existEmotion + ":" + emotion);
            }
        }
    }

    public void addSynonmy() throws IOException, InterruptedException {
        Map<String, String> words = emotionWordsManager.getAllWords();
        System.out.print(words + "\n");
        Iterator<Entry<String, String>> it = words.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> entry = (Map.Entry<String, String>) it
                    .next();
            /*
             * if (entry.getValue().equalsIgnoreCase("disgust") ||
             * entry.getValue().equalsIgnoreCase("fear") ||
             * entry.getValue().equalsIgnoreCase("surprise"))
             */
            addSynonym(entry.getKey(), entry.getValue());
        }

    }
}
