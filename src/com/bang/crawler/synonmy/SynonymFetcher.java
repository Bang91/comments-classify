package com.bang.crawler.synonmy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class SynonymFetcher {
    private static final String DICT_HOME = "http://www.iciba.com/";

    public List<String> fetch(String word) {
        List<String> res = new ArrayList<String>();
        try {
            Document doc = Jsoup.connect(DICT_HOME + word).get();
            Element tongyi = doc.select("div.tongyi div div").first();
            // Element tongyi = doc.select("#dict_content_3").first();
            // System.out.print(doc + "\n");
            if (tongyi == null) return res;
            Elements items = tongyi.select("ul.dl_show");
            if (items == null)
                return res;
            for (Element item : items) {
                Elements wordDiv = item.select("dl dd a");
                if (wordDiv == null)
                    return res;
                for (Element synonym : wordDiv) {
                    res.add(synonym.text());
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    }
    
    public List<String> fetchBaidu(String word) throws IOException, InterruptedException {
        final String home = "http://dict.baidu.com/s?wd=";
        List<String> res = new ArrayList<String>();
        
        Document doc = Jsoup.connect(home + word).timeout(9000).get();
        Thread.sleep(5000);
        Element tongyi = doc.select("#en-syn-ant dl").first();
        if (tongyi == null) return res;
        Elements items = tongyi.select(".links a");
        if (items == null) return res;
        for (Element item : items) {
            System.out.print(item.text() + "\n");
            res.add(item.text());
        }
        return res;
    }
}
