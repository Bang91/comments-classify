package com.bang.crawler.synonmy;

import java.io.IOException;

import com.bang.thelion.emotion.Emotion;
import com.bang.thelion.emotion.RedisTrainedWord;

public class Test {
    
    public static void main(String[] args) throws IOException, InterruptedException {
        EmotionWordExtender wordExtender = new EmotionWordExtender();
        // wordExtender.addSynonym("fealty", "sad");
        // wordExtender.addSynonym("sad", "disgust");
        wordExtender.addSynonmy();
        /**
         * Get the total words count of each emotion category.
         */
        /*
        RedisTrainedWord trainedWord = new RedisTrainedWord();
        Emotion emotions = new Emotion();
        for (String emotion : emotions.getEmotions()) {
            System.out.print(trainedWord.getFrequency(emotion) + ":" + emotion + "\n");
        }
        */
    }
}
