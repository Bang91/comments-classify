package com.bang.thelion.comment.lemma;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Comment implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private String company;
    private String msg;
    private String sentiment;
    
    List<String> words = new ArrayList<String>();
    
    Comment(String company, String msg, String sentiment){
        this.company = company;
        this.msg = msg;
        this.sentiment = sentiment;
    }
    
    Comment(String company, String msg, String sentiment, List<String> words) {
        this.company = company;
        this.msg = msg;
        this.sentiment = sentiment;
        words.addAll(words);
    }
    
    public String getCompany() {
        return this.company;
    }
    
    public String getMsg() {
        return this.msg;
    }
    
    public String getSentiment() {
        return this.sentiment;
    }
    
    public List<String> getWords() {
        return this.words;
    }
    
    public boolean addWord(String word) {
        return words.add(word);
    }
    
    public boolean removeWord(String word) {
        return words.remove(word);
    }
    
    public boolean addWordList(List<String> wordList) {
        return words.addAll(wordList);
    }
    
    public void removeAll() {
        words.clear();
    }
}
