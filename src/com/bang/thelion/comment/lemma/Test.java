package com.bang.thelion.comment.lemma;

import java.io.IOException;
import java.io.RandomAccessFile;
// import java.sql.SQLException;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

import com.bang.thelion.emotion.simple.vectorize.SimpleEmotionVectorTrainer;
// import java.util.Set;
// import java.util.List;

public class Test {

    public static void main(String[] args) throws IOException,
            ClassNotFoundException, SQLException {
        /*
         * RedisWords words = new RedisWords(); words.add("someone");
         * words.add("likes"); words.add("you"); words.add("you");
         * o
         * words.clear(); Set<String> set = words.getAll();
         * System.out.print(set.size());
         */
        /**
         * Save all word appear in comments to RedisWords.
         */
        /*
        RedisComments redisComment = new RedisComments();
        RedisWords redisWords = new RedisWords();
        Category category = new Category();
        for (String cat : category.getCategory()) {
            List<Comment> comments = redisComment.getAllByCategory(cat);
            for (Comment comm : comments) {
                for (String word : comm.getWords()) {
                    redisWords.add(word);
                    System.out.print(word + "\n");
                }
            }
        }
         */
        
        /**
         * Lemma the comments in MySql.
         */
        
        GetComment get = new GetComment("notagcomments");
        get.preprocess("");
        
        
        /**
         * Get all company names in Redis.
         */
        // Category category = new Category();
        /*
        RedisComments redisComments = new RedisComments();
        for (String cat : category.getCategory()) {
            Set<String> names = redisComments.getCompanyName(cat);
            RandomAccessFile frecord = new RandomAccessFile("CompanyNames",
                    "rw");
            long fileLength = frecord.length();
            frecord.seek(fileLength);
            frecord.writeBytes(names + "\n");
        }
        */
        
        /**
         * Get the msg and company name MAP to the id int trained docs.
         */
        /*
        RandomAccessFile frecord = new RandomAccessFile("RedisCommentMAPtoTrainedDocs",
                "rw");
        long fileLength = frecord.length();
        frecord.seek(fileLength);
        RedisComments redis = new RedisComments("strongbuy"); 
        // List<Comment> comment = redis.getAll(category.getCategory());
        /*
        for (int i = 1; i < comment.size(); i ++) {

            frecord.writeBytes(i + " " + comment.get(i).getCompany() + ":" + comment.get(i).getMsg() + "\n");
        }
        */
       /*
        Comment com = redis.getByCompanyAndMsg("Apple Computer", "1736");
        System.out.print(com.getSentiment() + com.getWords() + "\n");
        SimpleEmotionVectorTrainer trainer = new SimpleEmotionVectorTrainer();
        trainer.computeWeigh(com);
        */
        
        
        /*
         * RedisComments redis = new RedisComments(); 
         * List<Comment> comment =
         * redis.getAll(); System.out.print(comment.get(5).getWords().get(7)+
         * "\n");
         */
        /*
         * RedisComments redis = new RedisComments(); redis.clear(); //
         * System.out.print(set.toString()); // GetComment get = new
         * GetComment(); Comment m = new Comment("FB", "12", "Buy");
         * 
         * redis.insert(m); Comment comment = redis.getByCompanyAndMsg("FB",
         * "12"); System.out.print(comment.getSentiment() + "\n");
         * System.out.print("\nok!\n");
         */
        // for(Comment comment : comments)
        // System.out.print(comment.getCompany());
        // redis.remove(m);
        
      
        /**
         * Backup hash tables to <category>back hash tables.
         */
       /*
        String from = "buy";
        String to = "strongbuy";
        RedisComments moveComment = new RedisComments(from);
        RedisComments toComment = new RedisComments(to);
        
        List<Comment> all = moveComment.getAllByCategory(from);
        for (Comment comm : all) {
            toComment.insert(comm);
        }
        */
    }
}
