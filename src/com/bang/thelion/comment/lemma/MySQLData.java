package com.bang.thelion.comment.lemma;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MySQLData {
    private static final String URL = "jdbc:mysql://localhost/thelion";
    private static final String USER = "root";
    private static final String PASS = "123456";        
    private final Connection connMySQL;
    private static final String TABLE = "topics";// topics1209
    
    public ResultSet getAllFromMySQL(String category) throws SQLException {
        Statement stmt = connMySQL.createStatement();
        String query = "select company,msg,sentiment,detail from " + TABLE + " where tokenize = 0 and sentiment = " + "'" + category + "'" + ";";
        return stmt.executeQuery(query);
    }
    
    public MySQLData() throws ClassNotFoundException, SQLException {
        // connect to database.
        Class.forName("com.mysql.jdbc.Driver");
        connMySQL = DriverManager.getConnection(URL,USER,PASS);
    }
    
    public void updateTokenize(String company, String msg) throws SQLException {
        Statement stmt = connMySQL.createStatement();            
        String q = "update " + TABLE + " set tokenize = 1 where company = '" + company+ "' and msg = '" + msg +  "';"; 
        stmt.execute(q);
    }
    
    public void updataClassifyResult(String company, String msg, String cat) throws SQLException {
        Statement stmt = connMySQL.createStatement();
        company.replace("\'", "\\\'");
        company.replace("\"", "\\\"");
        String q = "update " + TABLE + " set tokenize = '" + cat + "' where company = '" + company+ "' and msg = '" + msg +  "';"; 
        stmt.equals(q);
    }

}
