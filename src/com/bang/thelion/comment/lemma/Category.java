package com.bang.thelion.comment.lemma;

import java.util.ArrayList;
import java.util.List;

public class Category {
    private final List<String> category = new ArrayList<String>();
    
    public Category() {
        category.add("strongbuy");
        category.add("buy");
        category.add("hold");
        category.add("short");
        category.add("sell");
        category.add("strongsell");
        category.add("scalp");
    }
    
    public List<String> getCategory() {
        return category;
    }
    
    public String getByIndex(int index) {
        return category.get(index);
    }
}
