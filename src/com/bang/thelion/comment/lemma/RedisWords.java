package com.bang.thelion.comment.lemma;

import java.util.Set;

import redis.clients.jedis.Jedis;

public class RedisWords {
    private static final String HOST = "localhost";
    private static final int PORT = 6379; 
    private static final String WORDS_KEY = "words";
    private static final String TRASH_KEY = "trash";
    final Jedis jedis;
    
    public RedisWords() {
        this.jedis = new Jedis(HOST, PORT);
    }
    
    public void add(String word) {
        jedis.sadd(WORDS_KEY, word);
    }
    
    public void remove(String word) {
        jedis.smove(WORDS_KEY, TRASH_KEY, word);
    }
    
    public Set<String> getAll() {
        return jedis.smembers(WORDS_KEY);
    }
    
    public void clearTrash() {
        long length = jedis.smembers(TRASH_KEY).size();
        while (length -- > 0) jedis.spop(TRASH_KEY);
    }
    
    public void clear() {
        long length = this.getAll().size();
        while (length -- > 0) jedis.spop(WORDS_KEY);
        clearTrash();
    }
}
