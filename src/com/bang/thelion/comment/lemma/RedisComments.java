package com.bang.thelion.comment.lemma;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.codec.binary.Base64;

import redis.clients.jedis.Jedis;

public class RedisComments {
    private final String REDIS_KEY;
    private static final String HOST = "localhost";
    private static final int PORT = 6379;
    final Jedis jedis;

    public static String transformToString(Comment m) throws IOException {
        final ByteArrayOutputStream byteArrayStream = new ByteArrayOutputStream();
        ObjectOutputStream objectStream = new ObjectOutputStream(
                byteArrayStream);
        objectStream.writeObject(m);
        objectStream.close();
        return new Base64().encodeToString(byteArrayStream.toByteArray());
    }

    public static Comment transformToComment(final String value)
            throws IOException, ClassNotFoundException {
        byte[] data = new Base64().decode(value);
        ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(
                data));
        Comment o = (Comment) ois.readObject();
        ois.close();
        return o;
    }

    public RedisComments() {
        this.REDIS_KEY = "comments";
        this.jedis = new Jedis(HOST, PORT);
    }

    public RedisComments(String category) {
        this.jedis = new Jedis(HOST, PORT);
        this.REDIS_KEY = category;
    }

    public void insert(Comment m) throws IOException {
        String field = m.getCompany() + ":" + m.getMsg();
        jedis.hset(REDIS_KEY, field, transformToString(m));
    }

    public List<Comment> getAll(List<String> category) {
        final List<Comment> comments = new ArrayList<Comment>();
        for (String cat : category) {
            List<Comment> commentValues = getAllByCategory(cat);
            comments.addAll(commentValues);
        }
        return comments;
    }

    public List<Comment> getAllByCategory(String cat) {
        final List<Comment> comments = new ArrayList<Comment>();
        final Collection<String> commentValues = jedis.hgetAll(cat).values();
        for (final String commentValue : commentValues) {
            try {
                comments.add(transformToComment(commentValue));
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return comments;
    }

    public Set<String> getCompanyName(String cat) {
        final Set<String> res = new HashSet<String>();
        Set<String> keys = jedis.hgetAll(cat).keySet();
        for (String key : keys) {
            String ss[] = new String[10];
            ss = key.split(":");
            res.add(ss[0]);
            System.out.print(ss[0] + "\n");
        }
        return res;
    }
    
    public Comment getByCompanyAndMsg(String company, String msg) {
        final String field = company + ":" + msg;
        final String value = jedis.hget(REDIS_KEY, field);

        if (value != null) {
            try {
                return transformToComment(value);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public void remove(String field) {
        jedis.hdel(REDIS_KEY, field);
    }

    public void remove(Comment m) {
        remove(m.getCompany() + ":" + m.getMsg());
    }

    public void clear() {
        Set<String> fields = jedis.hkeys(REDIS_KEY);
        Iterator<String> field = fields.iterator();
        for (; field.hasNext();) {
            remove(field.next());
        }
    }
}
