package com.bang.thelion.comment.lemma;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import weka.core.Stopwords;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

public class GetComment {
    RedisWords redisWords = new RedisWords();
    RedisComments redisComments;
    MySQLData mySQLData = new MySQLData();

    public List<Comment> preprocess(String mySqlCategory) throws IOException,
            SQLException {
        // List<Comment> comments = new ArrayList<Comment>();
        List<Comment> res = null;
        ResultSet all = mySQLData.getAllFromMySQL(mySqlCategory);

        // Print result into a file.
        String fileName = "wordList.txt";
        RandomAccessFile fw;
        fw = new RandomAccessFile(fileName, "rw");
        long fileLength = fw.length();
        fw.seek(fileLength);

        while (all.next()) {
            String company = all.getString("company");
            String msg = all.getString("msg");
            String sentiment = all.getString("sentiment");
            String detail = all.getString("detail");
            Comment comment = new Comment(company, msg, sentiment);

            

            String companyName = comment.getCompany();
            companyName.replace("\'", "\\\'");
            companyName.replace("\"", "\\\"");
            try {
                mySQLData.updateTokenize(companyName, comment.getMsg());
            } catch (SQLException e) {
                e.printStackTrace();
            }
            
            // Transform to lower case.
            detail = detail.toLowerCase();
            comment.addWordList(tokenize(detail));
            words.clear();

            System.out.print(comment.getWords());
            fw.writeBytes(comment.getCompany() + "||" + comment.getMsg() + "||"
                    + comment.getSentiment() + "\n");
            fw.writeBytes(comment.getWords() + "\n");

            redisComments.insert(comment);


            // comments.add(comment);
            comment.removeAll();
        }

        System.out.print("\nFinish\n");

        // return comments;
        return res;
    }

    private final Properties props;
    private final StanfordCoreNLP pipeline;

    public GetComment(String redisCat) throws ClassNotFoundException,
            SQLException {
        this.props = new Properties();
        props.put("annatators",
                "tokenize, ssplite, pos, lemma, ner, parse, dcoref");
        this.pipeline = new StanfordCoreNLP(props);

        this.redisComments = new RedisComments(redisCat);
    }

    // words is a temp variable.
    private List<String> words = new ArrayList<String>();

    public List<String> tokenize(String content) {
        Annotation document = new Annotation(content);
        this.pipeline.annotate(document);

        List<CoreMap> sentences = document.get(SentencesAnnotation.class);

        for (CoreMap sentence : sentences) {
            for (CoreLabel token : sentence.get(TokensAnnotation.class)) {
                String tt = token
                        .getString(CoreAnnotations.LemmaAnnotation.class);
                if (!(Stopwords.isStopword(tt))) {
                    words.add(tt);
                    redisWords.add(tt);
                }
            }
        }
        return words;
    }

    public String lemma(String word) {
        Annotation document = new Annotation(word);
        this.pipeline.annotate(document);

        List<CoreLabel> token = document.get(TokensAnnotation.class);
        String wordToken = token.get(0).getString(
                CoreAnnotations.LemmaAnnotation.class);
        System.out.print(wordToken + "\n");
        return wordToken;
    }
}
