package com.bang.thelion.comment.vectorization;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.bang.thelion.comment.lemma.RedisWords;

public class Test {
    public static void main(String[] args) {
        /*
        InformationGain ig = new InformationGain();
        ig.computeTotalNum();
        System.out.print(ig.getTotalNum());
        */
        /**
         *  Test existWord();
         */
        /*
        List<String> list = new ArrayList<String>();
        list.add("I"); list.add("OK"); list.add("23.55"); list.add("alj.gqwe.ga");
        String w = "content";
        if (ig.existWord(list, w)) System.out.print(w + " exits\n");
        else System.out.print(w + " not exists\n");
        */
        
        /**
         * Compute information gain value.
         */
        
        InformationGain ig = new InformationGain();
        ig.computeIG();
        // ig.reset();
        
        
        /**
         * Compute vector of a comment by TfIdf. 
         */
        
        /**
         * Get the first 500 words in RedisIGWords. 
         */
        /*
        CharacterSelector characterSelector = new CharacterSelector();
        try {
            characterSelector.putIntoFile("characters.txt", 500);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        */
        
        /**
         *  Test computeHC(), computeHCT().
         */
        /*
        System.out.print(ig.computeHC());
        System.out.print(ig.computeIGByWord(w));
        */
        
        /** 
         * Test categoryNum().
         */
        /*
        RedisWords redisWords = new RedisWords();
        Set<String> words = redisWords.getAll();
        
        for (String word : words) {
            System.out.print(word + "\n");
            ig.countCatNum(word);
            System.out.print(ig.getCategoryNum() + "\n");
            ig.clearCategoryNum();
        }
        */
       
    }
}
