package com.bang.thelion.comment.vectorization;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;

public class CharacterSelector {
    private RedisIGWords redisIGWords;
    private Set<String> characters;
    
    public CharacterSelector() {
        redisIGWords = new RedisIGWords();
    }
    
    public CharacterSelector(int num) {
        redisIGWords = new RedisIGWords();
        characters = redisIGWords.getFistNum(num);
    }
    public void putIntoFile(String fileName, int num) throws IOException {
        FileWriter fw = new FileWriter(fileName);
        BufferedWriter bf = new BufferedWriter(fw);
        bf.write(redisIGWords.getFistNum(num) + "\n");
        bf.close();
        fw.close();
    }
    
    public void setCharacter(int num){
        characters = redisIGWords.getFistNum(num);
    }
    
    public Set<String> getCharacter() {
        return characters;
    }
}
