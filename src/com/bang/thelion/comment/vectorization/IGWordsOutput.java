package com.bang.thelion.comment.vectorization;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;

import redis.clients.jedis.Tuple;

public class IGWordsOutput {
    private static RedisIGWords redisIGWords = new RedisIGWords();
    
    public void outputIntoFile(int num) throws IOException {
        FileWriter fw = new FileWriter("AllIGWordsOutput.txt");
        Set<Tuple> words = redisIGWords.getWithScore(num);
        for (Tuple word : words) {
            System.out.print(word.getElement() + "\t\t" + word.getScore() + "\n");
            fw.write(word.getElement() + "\t\t" + word.getScore() + "\n");
        }
        fw.close();
    }
    
    public static void main(String[] args) throws IOException {
        IGWordsOutput igWords = new IGWordsOutput();
        // igWords.outputIntoFile(350);
        igWords.outputIntoFile(-1);
    }

}
