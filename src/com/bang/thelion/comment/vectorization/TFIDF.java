package com.bang.thelion.comment.vectorization;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class TFIDF {
    private static final String READ_FILE_NAME = "comments.txt";
    private List<Map<String, Integer>> allCommentsTf = new ArrayList<Map<String, Integer>>();
    private Map<String, Integer> idf = new HashMap<String, Integer>();
    
    /*
    public List<String> readFile() {
        
    }
    */

    private Map<String, Integer> tf = new HashMap<String, Integer>();
    
    public Map<String, Integer> tf(List<String> words) {
        Iterator<String> word = words.iterator();
        while (word.hasNext()) {
            String key = word.next().toString();
            int num = tf.get(key);
            tf.put(key, num ++);
        }
        return tf;
    }
    
    public void allTf(List<List<String>> comments) {
        Iterator<List<String>> comment = comments.iterator();
        while (comment.hasNext()) {
            Map<String, Integer> words = tf(comment.next());
            allCommentsTf.add(words);
            tf.clear();
        }
    }
    
    public void idf(String word) {
        Iterator<Map<String, Integer>> commentTf = allCommentsTf.iterator();
        int count = 0;
        while (commentTf.hasNext()) {
            if(commentTf.next().get(word) > 0) count ++;
        }
        idf.put(word, count);
    }
    
    public void allIdf(List<String> allWords) {
        Iterator<String> word = allWords.iterator();
        while (word.hasNext()) {
            idf(word.next().toString());
        }
    }
    
    public void computeTfIdf() {
        Iterator<Map<String, Integer>> comment = allCommentsTf.iterator();
        int totalComments = allCommentsTf.size();
        while (comment.hasNext()) {
            Map<String, Integer> words = comment.next();
            int num = words.size();
            for(int i = 0; i < num; i ++) {
               // int wordNum = 
            }
        }
    }
}
