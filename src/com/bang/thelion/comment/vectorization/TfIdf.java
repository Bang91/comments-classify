package com.bang.thelion.comment.vectorization;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bang.thelion.comment.lemma.Comment;
import com.bang.thelion.comment.lemma.RedisComments;

public class TfIdf {
    /**
     * Get document set from RedisComments.
     */
    public Map<String, Double> IDF = new HashMap<String, Double>();
    private final List<Comment> DOCS;
    private static List<String> category = new ArrayList<String>();
    RedisComments redisComments = new RedisComments();
    
    public TfIdf() {
        category.add("strongbuy");
        category.add("buy");
        category.add("hold");
        category.add("short");
        category.add("sell");
        category.add("strongsell");
        category.add("scalp");

        DOCS = redisComments.getAll(category);
        // computeIdf(words);
    }
    
    /**
     * Compute IDF.
     * IDF = log (total number of document set / number of documents that <word> appears in)
     */
    boolean isAppear(Comment comment, String word) {
        for (String w : comment.getWords()) {
            if (word.equals(w)) return true;
        }
        return false;
    }
    
    public int appearDocNum(String word) {
        int count = 1;
        for (Comment comment : DOCS) {
            count += isAppear(comment, word) ? 1 : 0;
        }
        return count;
    }
    
    public double computeIdf(String word) {
        if (IDF.containsKey(word)) return IDF.get(word);
        double idf = Math.log((DOCS.size() * 1.0) / appearDocNum(word));
        IDF.put(word, idf);
        // System.out.print(word + " " + IDF.get(word) + "\n");
        return idf;
    }
    
    /**
     * Compute TF.
     * TF = frequency of <word> in <document> / total number of words in <document>  
     */
    public int frequency(Comment comment, String word) {
        int count  = 0;
        List<String> words = comment.getWords();
        for (String w : words) {
            if (word.equals(w)) count ++;
        }
        return count;
    }
    
    public double computeTf(Comment comment, String word) {
        return (frequency(comment, word) * 1.0) / (comment.getWords().size() + 1);
    }
    
    /**
     *  Compute each comment's TfIdf vector.
     */
    public double computeTfIdf(Comment comment, String word) {
        // double x = computeTf(comment, word) * computeIdf(word);
        double x = computeTf(comment, word) * computeIdf(word);
        // if (x > 0) System.out.print("\nvalue is " + x);
        return x;
    }
    
    public List<Double> computeVector(Comment comment, List<String> words) {
        List<Double> res = new ArrayList<Double>();
        // System.out.print(comment.getWords());
        for (String word : words) res.add(computeTfIdf(comment, word));
        return res;
    }
}
