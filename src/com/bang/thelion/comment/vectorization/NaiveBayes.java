package com.bang.thelion.comment.vectorization;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.bang.thelion.comment.lemma.Category;
import com.bang.thelion.comment.lemma.Comment;
import com.bang.thelion.comment.lemma.RedisComments;

public class NaiveBayes {
    private List<Double> categoryProb = new ArrayList<Double>();
    RedisComments redisComments = new RedisComments();
    Category cat = new Category();
    FileWriter fw;

    NaiveBayes() throws IOException {
        computeCategoryProb();
        fw = new FileWriter("naiveBayesOutput.txt");
    }
    

    private void computeCategoryProb() {
        List<String> cats = cat.getCategory();
        List<Comment> commentsAll = redisComments.getAll(cats);
        int total = commentsAll.size();
        for (String cat : cats) {
            List<Comment> comments = redisComments.getAllByCategory(cat);
            int count = comments.size();
            categoryProb.add((count * 0.1) / total);
        }
    }

    boolean isAppear(Comment comment, String word) {
        for (String w : comment.getWords()) {
            if (word.equals(w))
                return true;
        }
        return false;
    }

    public double computeWordProb(String word, String cat) throws IOException {
        // Get comments from redisComment.getByCategory()
        List<Comment> comments = redisComments.getAllByCategory(cat);
        int total = comments.size();
        int count = 0;
        for (Comment comment : comments) {
            count += isAppear(comment, word) ? 1 : 0;
        }
        fw.write(count + "\t");
        System.out.print(count + "\t");
        return (count * 1.0) / total;
    }

    public double computeWordsProb(List<String> words, String cat) throws IOException {

        double p = 1.0;
        for (String word : words) {
            double tmp = computeWordProb(word, cat);
            p *= tmp;
            fw.write(tmp + "\t");
            System.out.print(tmp + "\t");
        }
        fw.write("\n");
        System.out.print("\n");
        return p;
    }

    public int computeCommentProb(Comment comment) throws IOException {
        List<Double> prob = new ArrayList<Double>();
        List<String> cats = cat.getCategory();
        for (int i = 0; i < cats.size(); i++) {
            double p = computeWordsProb(comment.getWords(), cat.getByIndex(i))
                    * categoryProb.get(i);
            prob.add(p);
        }
        fw.write(prob + "\n");
        System.out.print(prob + "\n");
        return max(prob);
    }

    public int max(List<Double> prob) {
        int index = 0;
        double max = 0.0;
        for (int i = 0; i < prob.size(); i++) {
            if (max < prob.get(i)) {
                index = i;
                max = prob.get(i);
            }
        }
        return index;
    }

    public String category(Comment comment) throws IOException {
        fw.write(comment.getWords() + "\n");
        System.out.print(comment.getWords() + "\n");
        int index = computeCommentProb(comment);
        return cat.getByIndex(index);
    }
    
    public void close() throws IOException {
        fw.close();
    }

}
