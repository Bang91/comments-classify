package com.bang.thelion.comment.vectorization;

import java.util.Iterator;
import java.util.Set;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Tuple;

public class RedisIGWords {
    private static final String HOST = "localhost";
    private static final int PORT = 6379; 
    private String WORDS_KEY = "igwords";

    final Jedis jedis;
    
    public RedisIGWords() {
        this.jedis = new Jedis(HOST, PORT);
    }
    
    public RedisIGWords(String redisKey) {
        this();
        // TODO Auto-generated constructor stub
        this.WORDS_KEY = redisKey;
    }

    public void add(double score, String member) {
        jedis.zadd(WORDS_KEY, score, member);
    }
    
    public Set<String> getAll() {
        return getRange(0, -1);
    }
    
    public Set<String> getRange(int start, int end) {
        return jedis.zrevrange(WORDS_KEY, start, end);
    }
    
    public Set<String> getFistNum(int num) {
        return getRange(0, num);
    }
    
    public double get(String member) {
        if(jedis.zrank(WORDS_KEY, member) == null) return 0;
        else {
            double res = jedis.zscore(WORDS_KEY, member);
            return res;
        }
    }
    
    public Set<Tuple> getWithScore(int num) {
        return jedis.zrevrangeWithScores(WORDS_KEY, 0, num);
    }
    public void remove(String member) {
        jedis.zrem(WORDS_KEY, member);
    }
    
    public void clear() {
        Set<String> members = getAll();
        Iterator<String> member = members.iterator();
        for (; member.hasNext();) {
            remove(member.next());
        }
    }
}
