package com.bang.thelion.comment.vectorization;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.bang.thelion.comment.lemma.Comment;
import com.bang.thelion.comment.lemma.RedisComments;
import com.bang.thelion.comment.lemma.RedisWords;

public class InformationGain {
    private final List<Integer> totalNum = new ArrayList<Integer>();
    private List<Integer> categoryNum = new ArrayList<Integer>();
    private static List<String> category = new ArrayList<String>();
    
    public InformationGain() {
        category.add("strongbuy");
        // category.add("buy");
        category.add("hold");
        category.add("short");
        // category.add("sell");
        category.add("strongsell");
        category.add("scalp");
    }
    
    private RedisComments redis = new RedisComments();
    
    /**
     * Compute H(C).
     * H(C) => -P(Ci) * log2(P(Ci)
     * P(Ci) = document number of category i / total number of document set
     */
    public void computeTotalNum() {
        for (int i = 0; i < category.size(); i ++) {
            totalNum.add(redis.getAllByCategory(category.get(i)).size());
        }
    }
    
    public boolean existWord(List<String> wordList, String word) {
        for (String w : wordList) {
            if (w.endsWith(word)) return true;
        }
        return false;
    }
    
    public void countCatNum(String word) {
        for (int i = 0; i < category.size(); i ++) {
            List<Comment> comments = redis.getAllByCategory(category.get(i));
            int count = 0;
            for (Comment comment : comments) {
                if (existWord(comment.getWords(), word)) count ++;
            }
            categoryNum.add(count);
        }
    }
    
    private double hc = 0.0;
    private int totalDoc = 0;
    private int Base = 2;
    
    public double computeHC() {
        hc = 0.0;
        for (int i = 0; i < totalNum.size(); i ++) {
            totalDoc += totalNum.get(i);
            // System.out.print(totalDoc + "\n");
        }
        for (int i = 0; i < totalNum.size(); i ++) {
            double p = totalNum.get(i) / (totalDoc * 1.0);
            hc -= p * ( Math.log(p) / Math.log(Base));
            // System.out.print(hc + " " + p + "\n");
        }
        return hc;
    }
    
    /**
     * Compute H(C|T).
     * H(C|T) = P(t) * H(C|t) + p(-t) * H(C|t)
     */
    public double computeHCT(String word) {
        countCatNum(word);
        int total = 0;
        double hct = 0.0, hcnt = 0.0;
        for (int i = 0; i < categoryNum.size(); i ++) {
            total += categoryNum.get(i);
        }
        System.out.print("\n" + total + "\n");
        for (int i = 0; i < categoryNum.size(); i ++) {
            double p = (categoryNum.get(i)) / (total * 1.0  + 1.0);
            double pnt = (totalNum.get(i) - categoryNum.get(i)) / (totalDoc - total + 1.0) ;

            System.out.print(p + " " + pnt + "\n");
            if(p != 0) hct -= p * Math.log(p) / Math.log(Base);
            else hct = 0;
            if(pnt != 0) hcnt -= pnt * Math.log(pnt) / Math.log(Base);
            else hcnt = 0;
        }
        clearCategoryNum();
        System.out.print(hct + hcnt + "\n" +  totalDoc + "\n");
        return hct * (total / (totalDoc * 1.0 )) + hcnt * (totalDoc - total) / (totalDoc * 1.0 );
    }

    /**
     * Compute IG.
     * IG(T) = H(C) - H(C|T)
     */
    public double computeIGByWord(String word) {
        double tmp = computeHCT(word);
        System.out.print(word + ":" + tmp + "\n");
        return hc - tmp;
    }
    
    RedisWords redisWords = new RedisWords();
    RedisIGWords redisIGWords = new RedisIGWords();
    
    public void computeIG() {
        computeTotalNum();
        computeHC();
        // TODO: Store the emotion words into a SET.
        Set<String> words = redisWords.getAll();
        for (String word : words) {
            if (redisIGWords.get(word) == -1)
                redisIGWords.add(computeIGByWord(word), word);
        }
    }
    
    public void reset() {
        redisIGWords.clear();
    }
    public void clearCategoryNum() {
        categoryNum.clear();
    }

    public List<Integer> getTotalNum() {
        return totalNum;
    }

    public List<Integer> getCategoryNum() {
        return categoryNum;
    }

    public void setCategoryNum(List<Integer> categoryNum) {
        this.categoryNum = categoryNum;
    }
}
