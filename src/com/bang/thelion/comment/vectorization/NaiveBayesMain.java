package com.bang.thelion.comment.vectorization;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bang.thelion.comment.lemma.Comment;
import com.bang.thelion.comment.lemma.RedisComments;

public class NaiveBayesMain {
    public static void main(String[] args) throws IOException {
        NaiveBayes naiveBayes = new NaiveBayes();
        RedisComments redisComment = new RedisComments();
        Map<String, Integer> categoryNum = new HashMap<String, Integer>();
        String cat = "short";
        List<Comment> list = redisComment.getAllByCategory(cat);
        FileWriter fw = new FileWriter("naiveBayesOutput.txt");
        
        for (Comment comment : list) {
            if (comment.getWords().size() == 0) continue;
            String res = naiveBayes.category(comment);
            if (categoryNum.containsKey(res)) {
                int count = categoryNum.get(res) + 1;
                categoryNum.put(res, count);
            } else categoryNum.put(res, 1);
            System.out.print(categoryNum + "\n");
            System.out.print("The classified result is : " + res + "\n\n");
        }
        fw.write(categoryNum + "\n");
        naiveBayes.close();
    }
}
