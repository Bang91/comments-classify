package com.bang.thelion.comment.weka;

import java.io.IOException;

import com.bang.thelion.comment.lemma.RedisWords;

public class AddArffHeader {
    public static void main(String[] args) throws IOException {
        ArffFile arffHeader = new ArffFile("header");
        RedisWords redisWords = new RedisWords();
        // TODO: Adjust K = 100.
        arffHeader.addAttribute(100);
        arffHeader.close();
    }
}
