package com.bang.thelion.comment.weka;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.bang.thelion.comment.lemma.Comment;
import com.bang.thelion.comment.lemma.RedisWords;
import com.bang.thelion.comment.vectorization.RedisIGWords;
import com.bang.thelion.comment.vectorization.TfIdf;

public class VectorComputer {
    private final List<String> character;
    
    public VectorComputer() {
        RedisWords redisWords = new RedisWords();
        // RedisIGWords emotionIgWords = new RedisIGWords("emotionIgWords");
        // character = transformToList(redisWords.getAll());
        RedisIGWords emotionIgWords = new RedisIGWords("igwords");
        //TODO: Replace redisWords to IG words.
        character = transformToList(emotionIgWords.getFistNum(120));
        tfIdf = new TfIdf();
    }
    public VectorComputer(int num) {
        RedisIGWords redisIGWords = new RedisIGWords();
        character = transformToList(redisIGWords.getFistNum(num));
    }
    
    public List<String> transformToList(Set<String> set) {
        List<String> res = new ArrayList<String>();
        for (String s : set) {
            res.add(s);
        }
        return res;
    }
    
    private TfIdf tfIdf;
    
    public List<Double> computeTfIdf(Comment comment) {
        return tfIdf.computeVector(comment, character);
    }
}
