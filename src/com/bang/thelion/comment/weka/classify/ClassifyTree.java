package com.bang.thelion.comment.weka.classify;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Random;

import com.bang.thelion.comment.lemma.Comment;
import com.bang.thelion.comment.lemma.MySQLData;
import com.bang.thelion.comment.lemma.RedisComments;
import com.bang.thelion.comment.weka.ArffFile;
import com.bang.thelion.emotion.RedisEmotionWordsManager;
import com.bang.thelion.emotion.simple.vectorize.SimpleEmotionVectorTrainer;

import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.functions.SMO;
import weka.classifiers.trees.J48;
import weka.core.Instances;

public class ClassifyTree {
    private Instances instances;

    public void getFileInstances(String fileName) throws IOException {
        FileReader file = new FileReader(fileName);
        System.out.print("Reading from file ......\n");
        instances = new Instances(file);
        System.out.print("Read all\n");
        instances.setClassIndex(0);
    }

    public J48 classifyTree() throws Exception {
        J48 classifier = new J48();
        classifier.buildClassifier(instances);
        return classifier;
    }

    public J48 classify() throws Exception {
        FileWriter fileWriter = new FileWriter("ClassifyTreeOutput.txt");
        J48 classifier = classifyTree();
        // NaiveBayes classifier = new NaiveBayes();
        // SMO classifier = new SMO();

        Evaluation eval = new Evaluation(instances);
        // eval.evaluateModel(classifier, instances );
        eval.crossValidateModel(classifier, instances, 10, new Random(1));
        fileWriter.write(eval.toClassDetailsString() + "\n\n");
        fileWriter.write(eval.toSummaryString() + "\n\n");
        fileWriter.write(eval.toMatrixString() + "\n\n");
        System.out.println(eval.toClassDetailsString());
        System.out.println(eval.toSummaryString());
        System.out.println(eval.toMatrixString());
        /*
         * Evaluation eval = new Evaluation( instances );
         * eval.crossValidateModel( classifier, instances, 10, new Random(1));
         */
        /*
         * for (int i = 0; i < num; i++) {
         * fileWriter.write("The classify result of " + i + "is :" +
         * classifier.classifyInstance(instances.get(i)) + "\n"); }
         */

        fileWriter.close();
        return classifier;
    }

    FileWriter fileWriter;

    /**
     * Classify only one comment.
     */
    public void classifyInstances(J48 classifier, Comment comment,
            String fileName) throws Exception {
        FileReader file = new FileReader(fileName);
        Instances testInstances = new Instances(file);

        testInstances.setClassIndex(0);
        RedisEmotionWordsManager redisResult = new RedisEmotionWordsManager(
                "classifyresult");

        System.out.print("\n" + testInstances.size() + "\n");
        for (int i = 0; i < testInstances.size(); i++) {
            double res = classifier.classifyInstance(testInstances.get(i));
            String cat = null;
            if (res - 1 < 0)
                cat = "buy";
            if (res - 1 == 0)
                cat = "hold";
            if (res - 1 > 0)
                cat = "sell";
            redisResult.add(comment.getCompany() + ":" + comment.getMsg(), cat);

            fileWriter.write(comment.getCompany() + "," + comment.getMsg()
                    + "," + cat + "\n");

            System.out.print(comment.getCompany() + ":" + comment.getMsg()
                    + "," + cat + "\n");
        }

    }


    SimpleEmotionVectorTrainer trainer = new SimpleEmotionVectorTrainer();
    int i = 0;
    public void classifyComment(J48 classifier, Comment comment)
            throws Exception {
        ArffFile file = new ArffFile("arffDocs/TestOneCommentVector");
        
        System.out.print("Compute weight ..." + "\n");
        List<Double> weight = trainer.train(comment);
        System.out.print("Finish Compute weight " + "\n");
        
        file.addHeader();
        file.addData("notagcomments", weight);
        file.close();
        
        System.out.print("Classify comment ... " + "\n");
        classifyInstances(classifier, comment, "arffDocs/TestOneCommentVector.arff");
        System.out.print("Finish Classify comment " + "\n");
        
        System.out.print("Finish the " + i ++ + " comment\n");
    }

    /**
     * @throws Exception
     * 
     */
    /*
     * public void updateResult() throws ClassNotFoundException, SQLException,
     * IOException { FileWriter fileWriter = new
     * FileWriter("SortedClassifyResultOutput.csv"); RedisEmotionWordsManager
     * redisResult = new RedisEmotionWordsManager( "classifyresult"); MySQLData
     * mySQLData = new MySQLData(); ResultSet resultSet =
     * mySQLData.getAllFromMySQL("");
     * 
     * while (resultSet.next()) { String cm = resultSet.getString("company") +
     * ":" + resultSet.getString("msg"); fileWriter.write(redisResult.getCat(cm)
     * + ",\n"); } }
     */

    public void classifyComments(ClassifyTree classifyTree) throws Exception {
        getFileInstances("SimpleEmotionVectorize.arff");
        J48 classifier = classify();

        fileWriter = new FileWriter("ClassifyResultOutput.txt");

        RedisComments redis = new RedisComments("notagcomments");
        List<Comment> comments = redis.getAllByCategory("notagcomments");
        for (Comment comment : comments) {
            classifyTree.classifyComment(classifier, comment);
            Thread.sleep(100);
        }

        fileWriter.close();
    }

    public static void main(String[] args) throws Exception {
        // classifyTree.getFileInstances("contact-lenses.arff");
        // classifyTree.classifyInstances(classifyTree.classify(),
        // "TestVector.arff");
        // classifyTree.updateResult();
        ClassifyTree classifyTree = new ClassifyTree();
        classifyTree.classifyComments(classifyTree);

    }
}
