package com.bang.thelion.comment.weka;

import java.io.IOException;
import java.util.List;

import com.bang.thelion.comment.lemma.Category;
import com.bang.thelion.comment.lemma.Comment;
import com.bang.thelion.comment.lemma.RedisComments;

public class Test {
    public static void main(String[] args) throws IOException {
        RedisComments redisComments = new RedisComments();
        // String cat = "strongsell";
        Category category = new Category();
        List<String> cats = category.getCategory();
        VectorComputer vectorComputer = new VectorComputer();
        ArffFile file = new ArffFile("TfIdfOutput");
        /*
        String cat = "short";
        List<Comment> comments = redisComments.getAllByCategory(cat);
        for (Comment comment : comments) {
            file.addData(cat, vectorComputer.computeTfIdf(comment));
            System.out.print("\nComplete!\n");
        }
        */
        
        for (String cat : cats) {
            List<Comment> comments = redisComments.getAllByCategory(cat);
            // for (Comment comment : comments)
            // System.out.print(comment.getWords());

            // Category category = new Category();
            // System.out.print(category.getCategory());

            for (Comment comment : comments) {
                
                if (cat.equalsIgnoreCase("strongbuy") || cat.equalsIgnoreCase("scalp") ) cat = "buy";
                if (cat.equalsIgnoreCase("Short") || cat.equalsIgnoreCase("StrongSell")) cat = "sell";
                
                file.addData(cat, vectorComputer.computeTfIdf(comment));
                System.out.print("\nComplete!\n");
            }
        }
       
        file.close();
        
        /*
         * Test Double to String.
         */
        /*
         * Double d = 12.546543; System.out.print(d.toString());
         */
    }

}
