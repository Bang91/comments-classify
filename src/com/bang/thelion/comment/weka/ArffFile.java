package com.bang.thelion.comment.weka;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

public class ArffFile {
    private static final String FORMAT = ".arff";
    private final String FILE_NAME;
    private FileWriter fw;
    private BufferedWriter bf;
    private RandomAccessFile rfw; 
    
    public ArffFile(String fileName) throws IOException {
        FILE_NAME = fileName + FORMAT;
        fw = new FileWriter(FILE_NAME);
        bf = new BufferedWriter(fw);
        rfw= new RandomAccessFile(FILE_NAME, "rw");
    }
    
    private String transformToArffData(List<String> vector) {
        String res = null;
        int count = 0, len = vector.size();
        for (String vect : vector) {
            if (count == 0) res = vect + ",";
            else {
                if (count == len - 1) res += vect;
                else res += vect + ",";
            }
            count ++;
        }
        return res;
    }
    
    private String transformToArffHeader(String attribute, String type) {
        return "@ATTRIBUTE " + attribute + " " + type;
    }
    
    public void addAttribute(int num) throws IOException {
        String type = "NUMERIC";
        for (int i = 0; i < num; i ++) addContent(transformToArffHeader(i + "", type));
    }
    
    public void addData(String tag, List<Double> values) throws IOException {
        List<String> list = new ArrayList<String>();
        list.add(tag);
        for (Double value : values) {
            // if (value != 0) System.out.print("\n" + value + "\n");
            list.add(value.toString());
        }
        addData(list);
    }
    public void addData(List<String> vector) throws IOException {
        addContent(transformToArffData(vector));
    }
    

    public void write(String content) throws IOException {
        bf.write(content);
    }
    
    public void addContent(String content) throws IOException {
           // long fileLength = rfw.length();
           // rfw.seek(fileLength);
           rfw.writeBytes(content);
           rfw.writeBytes("\n");
           // System.out.print(content);
    }   
    
    public void addHeader() throws IOException {
        // long fileLength = rfw.length();
        // rfw.seek(fileLength);
        rfw.writeBytes("@RELATION thelion" + "\n");
        rfw.writeBytes("@attribute sentiment {buy,hold,sell,notagcomments}" + "\n");
        rfw.writeBytes("@attribute anger real" + "\n");
        rfw.writeBytes("@attribute disgust real" + "\n");
        rfw.writeBytes("@attribute fear real" + "\n");
        rfw.writeBytes("@attribute joy real" + "\n");
        rfw.writeBytes("@attribute sadness real" + "\n");
        rfw.writeBytes("@attribute surprise real" + "\n");
        rfw.writeBytes("@data" + "\n");
    }
    
    public void close() throws IOException {
        fw.close();
        bf.close();
        rfw.close();
    }
}
