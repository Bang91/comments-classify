package com.bang.thelion.emotion.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.bang.thelion.comment.lemma.Comment;
import com.bang.thelion.comment.lemma.RedisComments;
import com.bang.thelion.emotion.Emotion;
import com.bang.thelion.emotion.RedisEmotionWordsManager;
import com.bang.thelion.emotion.RedisTrainedWord;
import com.bang.thelion.emotion.reliability.EmotionReliableWordTrainer;
import com.bang.thelion.emotion.reliability.RedisEmotionReliableWord;

public class TrainingDataBuilder {
    private static final String NULL_EMOTION_CAT = "nullEmotion";
    
    private WekaDataUtil dataUtil;
    private RedisComments redisComments = new RedisComments();
    private List<String> emotions;

    private RedisEmotionWordsManager emotionWordsManager = new RedisEmotionWordsManager("wordnet");
    private RedisEmotionWordsManager wordnetOriginManager = new RedisEmotionWordsManager("wordnetOrigin");
    private RedisTrainedWord trainedWordManager = new RedisTrainedWord();
    private RedisEmotionReliableWord reliableWordManager = new RedisEmotionReliableWord(); 

    public TrainingDataBuilder(String fileName) throws IOException {
        dataUtil = new WekaDataUtil(fileName);
        Emotion emotion = new Emotion();
        emotions = emotion.getEmotions();
    }

    public List<Double> computeIFIDF(List<String> words) {
        List<Double> res = new ArrayList<Double>();
        Map<String, Integer> fre = new HashMap<String, Integer>();

        for (String word : words) {
            int count = 1;
            String emotion = emotionWordsManager.getCat(word);
            // if (emotion == null)
            //    emotion = trainedWordManager.getEmotion(word);
            if (emotion != null) {
                if (fre.containsKey(emotion))
                    count += fre.get(emotion);
                fre.put(emotion, count);
            }
        }

        for (int i = 0; i < emotions.size(); i++) {
            int total = emotionWordsManager.getFrequency(emotions.get(i))
                    + trainedWordManager.getFrequency(emotions.get(i));
            System.out.print(total + "\n");
            if (fre.containsKey(emotions.get(i))) {
                res.add( (fre.get(emotions.get(i)) * 1.0) / (total + 1.0) ); 
            } else res.add(0.0);
        }
        return res;
    }
    
    public void build(String category) throws IOException, ClassNotFoundException {
        buildHeader();
        buildData(category);
    }
    
    public void buildHeader() throws IOException {
        dataUtil.addHeader(emotions);
    }
    
    List<Double> computeReliability(List<String> words) throws IOException, ClassNotFoundException {
        EmotionReliableWordTrainer trainer = new EmotionReliableWordTrainer();
        Map<String, Double> resMap = new HashMap<String, Double>();
        for (String emotion : emotions) {
            resMap.put(emotion, 0.0);
        }
        resMap.put(NULL_EMOTION_CAT, 0.0);
        Double total = 0.0;
        
        for (String word : words) {
            Map<String, Double> weigh = trainer.computeReliability(word);
            
            Iterator<Entry<String, Double>> weighIt = weigh.entrySet()
                    .iterator();
            while (weighIt.hasNext()) {
                Map.Entry<String, Double> weighEntry = (Map.Entry<String, Double>) weighIt
                        .next();
                double weighTemp = resMap.get(weighEntry.getKey());
                weighTemp += weighEntry.getValue();
                total += weighEntry.getValue();
                resMap.put(weighEntry.getKey(), weighTemp);
            }
        }
        
        if (total == 0) total = 1.0;
        List<Double> res = new ArrayList<Double>();
        for (String emotion : emotions) {
            double tmp = resMap.get(emotion) / total;
            res.add(tmp);
        }
        double tmp = resMap.get(NULL_EMOTION_CAT) / total;
        res.add(tmp);
        return res;
    }
    
    public void buildData(String category) throws IOException, ClassNotFoundException {
        List<Comment> comments = redisComments.getAllByCategory(category);
        System.out.print(comments.size() + "\n");
        for (Comment comment : comments) {
            System.out.print(comment.getWords() + "\n");
            
            /**
             * Compute vector by TfIdf.
             */
            // List<Double> values = computeIFIDF(comment.getWords());
            
            /**
             * Compute vector by words reliability.
             */
            List<Double> values = computeReliability(comment.getWords());
            
            System.out.print(values + "\n");
            dataUtil.addData(category, comment.getWords().size(), values);
        }
    }
}
