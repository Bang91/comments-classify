package com.bang.thelion.emotion.data;

import java.io.IOException;

import com.bang.thelion.comment.lemma.Category;

public class Test {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        TrainingDataBuilder trainingDataBuilder = new TrainingDataBuilder("TrainingData");
        trainingDataBuilder.buildHeader();
        
        Category cat = new Category();
        for (String category : cat.getCategory()) {
            trainingDataBuilder.buildData(category);
        }
    }
}
