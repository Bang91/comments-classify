package com.bang.thelion.emotion.data;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.List;

import com.bang.thelion.comment.lemma.Category;

public class WekaDataUtil {
    private static final String FORMAT = ".arff";
    private final String FILE_NAME;
    private FileWriter fw;
    private BufferedWriter bf;
    private RandomAccessFile rfw; 
    
    private String transformToArffHeader(String attr) {
        return "@attribute " + attr + " real\n";
    }
    
    private String transformToArffData(String tag, int size, List<Double> values) {
        String res = tag + "," + size;
        // String res = tag;
        for (Double value : values) {
            res += "," + String.format("%.8f", value);
        }
        res += "\n";
        return res;
    }
    
    private String transformToArffHeaderCategory(List<String> categories) {
        String res = "@attribute " + "sentiment {";
        for (int i = 0; i < categories.size(); i ++) {
            if (i == categories.size() - 1) {
                res += categories.get(i) + "}\n";
            }
            else res += categories.get(i) + ",";
        }
        return res;
    }
    
    public WekaDataUtil(String fileName) throws IOException {
        FILE_NAME = fileName + FORMAT;
        fw = new FileWriter(FILE_NAME);
        bf = new BufferedWriter(fw);
        rfw= new RandomAccessFile(FILE_NAME, "rw");
    }
    
    public void addHeader(List<String> attrs) throws IOException {
        addContent("@relation thelion\n\n");
        Category cat = new Category();
        addContent(transformToArffHeaderCategory(cat.getCategory()));
        addContent("@attribute size NUMERIC\n");
        for (String attr : attrs) {
            addContent(transformToArffHeader(attr));
        }
        addContent("\n@data\n");
    }
    
    public void addData(String tag, int size, List<Double> values) throws IOException {
        if (tag.equalsIgnoreCase("strongbuy") || tag.equalsIgnoreCase("scalp") ) tag = "buy";
        if (tag.equalsIgnoreCase("Short") || tag.equalsIgnoreCase("StrongSell")) tag = "sell";
        addContent(transformToArffData(tag.toLowerCase(), size, values));
    }
    
    public void addContent(String content) throws IOException {
        long fileLength = rfw.length();
        rfw.seek(fileLength);
        rfw.writeBytes(content);
    }
    
    public void close() throws IOException {
        fw.close();
        bf.close();
        rfw.close();
    }
}
