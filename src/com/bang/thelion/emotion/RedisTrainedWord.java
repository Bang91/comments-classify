package com.bang.thelion.emotion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import redis.clients.jedis.Jedis;

public class RedisTrainedWord {
    private static final String DEFAULT_REDIS_HOST = "localhost";
    private static final int DEFAULT_REDIS_PORT = 6379;
    private static final String REDIS_WORDTRAINED_KEY = "wordTrained";

    private final Jedis jedis;
    private final String host;
    private final int port;

    private Map<String, Integer> frequency = new HashMap<String, Integer>();

    public RedisTrainedWord() {
        this(DEFAULT_REDIS_HOST, DEFAULT_REDIS_PORT);

    }

    public RedisTrainedWord(String host, int port) {
        this.host = host;
        this.port = port;
        this.jedis = new Jedis(host, port);
    }

    public void add(String word, String emotion) {
        if (emotion != null)
            jedis.hset(REDIS_WORDTRAINED_KEY, word, emotion);
    }

    public String getEmotion(String word) {
        return jedis.hget(REDIS_WORDTRAINED_KEY, word);
    }

    public void remove(String field) {
        jedis.hdel(REDIS_WORDTRAINED_KEY, field);
    }

    public void computeFrequency() {
        Map<String, String> map = jedis.hgetAll(REDIS_WORDTRAINED_KEY);
        Iterator<Entry<String, String>> it = map.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry<String, String> entry = (Map.Entry<String, String>) it
                    .next();
            if (frequency.containsKey(entry.getValue())) {
                frequency.put(entry.getValue(),
                        frequency.get(entry.getValue()) + 1);
            } else
                frequency.put(entry.getValue(), 1);
        }
    }

    public int getFrequency(String emotion) {
        if (!frequency.containsKey(emotion))
            computeFrequency();
        if (frequency.containsKey(emotion))
            return frequency.get(emotion);
        else
            return 0;
    }

    public List<String> getByEmotion(String emotion) {
        List<String> res = new ArrayList<String>();
        Map<String, String> map = jedis.hgetAll(REDIS_WORDTRAINED_KEY);
        Iterator<Entry<String, String>> it = map.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry<String, String> entry = (Map.Entry<String, String>) it
                    .next();
            if (entry.getValue().equalsIgnoreCase(emotion)
                    && entry.getKey().indexOf("http") < 0
                    && entry.getKey().indexOf("#") < 0)
                res.add(entry.getKey());
        }
        System.out.print(res);
        return res;
    }
    

    
    public void clear() {
        Set<String> fields = jedis.hkeys(REDIS_WORDTRAINED_KEY);
        Iterator<String> field = fields.iterator();
        for (; field.hasNext();) {
            remove(field.next());
        }
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }
}
