package com.bang.thelion.emotion.similarity;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class CosineSimilarityTest {
    CosineSimilarity cosineSimilarity;
    List<Double> vectorA = new ArrayList<Double>();
    List<Double> vectorB = new ArrayList<Double>();
    
    @Before
    public void setUp() {
        cosineSimilarity = new CosineSimilarity();
        for (int i = 0; i < 3; i ++) {
            vectorA.add(i * 1.0);
            vectorB.add(i * 2.0);
        }
    }

    @Test
    public void testSum() {
        double sum = cosineSimilarity.sum(vectorA, 3);
        assertEquals(3, sum, 0.0001);
    }
    
    @Test
    public void testQuadraticSum() {
        double sum = cosineSimilarity.quadraticSum(vectorA, 3);
        assertEquals(5, sum, 0.0001);
    }
    
    @Test 
    public void testCrossProduct() {
        double sum = cosineSimilarity.crossProduct(vectorA, vectorB, 3);
        assertEquals(10, sum, 0.0001);
    }

    @Test
    public void testAverage() {
        double average = cosineSimilarity.average(vectorA, 3);
        assertEquals(1, average, 0.0001);
    }
    
    @Test
    public void testUpdatedSqrt() {
        double sum = cosineSimilarity.updatedSqrt(vectorA, 3);
        assertEquals(Math.sqrt(2), sum, 0.0001);
    }
    
    @Test
    public void testCosine() {
        double sum = cosineSimilarity.cosine(vectorA, vectorB, 3);
        assertEquals(1, sum, 0.0001);
    }
}
