package com.bang.thelion.emotion.similarity;

import java.util.List;

public class CosineSimilarity {
    public double sum(List<Double> vector, int size) {
        double res = 0;
        for (int i = 0; i < size; i++) {
            res += vector.get(i);
        }
        return res;
    }

    public double quadraticSum(List<Double> vector, int size) {
        return crossProduct(vector, vector, size);
    }

    public double average(List<Double> vector, int size) {
        return sum(vector, size) / size;
    }

    public double crossProduct(List<Double> vectorA, List<Double> vectorB,
            int size) {
        double res = 0;
        for (int i = 0; i < size; i++) {
            res += vectorA.get(i) * vectorB.get(i);
        }
        return res;
    }

    public double updatedSqrt(List<Double> vector, int size) {
        return Math.sqrt(quadraticSum(vector, size) - average(vector, size)
                * sum(vector, size));
    }

    public double cosine(List<Double> vectorA, List<Double> vectorB, int size) {
        double averageB = average(vectorB, size);
        double sumA = sum(vectorA, size);
        double numerator = crossProduct(vectorA, vectorB, size) - averageB
                * sumA;
        double denominator = updatedSqrt(vectorA, size) * updatedSqrt(vectorB, size);
        if (denominator != 0) return numerator / denominator;
        return -1;
    }
}
