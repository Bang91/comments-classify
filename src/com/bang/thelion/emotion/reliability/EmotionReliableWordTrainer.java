package com.bang.thelion.emotion.reliability;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import com.bang.thelion.comment.lemma.Category;
import com.bang.thelion.comment.lemma.Comment;
import com.bang.thelion.comment.lemma.RedisComments;
import com.bang.thelion.comment.lemma.RedisWords;
import com.bang.thelion.emotion.Emotion;
import com.bang.thelion.emotion.RedisEmotionWordsManager;
import com.bang.thelion.emotion.RelativityComputer;

public class EmotionReliableWordTrainer {
    private static final String REDIS_WORDNETORIGIN_KEY = "wordnetOrigin";
    private static final String REDIS_WORDNET_KEY = "sysnonymwords";

    private static final String NULL_EMOTION_CAT = "nullEmotion";
    private static final String OVERLAP_VALUE = "overlap";
    private static final double WORDNET_WEIGH = 1.0;
    private static final double SYNONMY_COEFF = 0.8;
    private static final double TRAINED_COFF = 0.6;

    private RedisEmotionReliableWord trainedWord = new RedisEmotionReliableWord();
    private RedisEmotionWordsManager wordnetWord = new RedisEmotionWordsManager(
            REDIS_WORDNET_KEY);
    private RedisEmotionWordsManager wordnetOriginWord = new RedisEmotionWordsManager(
            REDIS_WORDNETORIGIN_KEY);
    private RelativityComputer relativityComputer = new RelativityComputer();
    private Emotion emotionCategory = new Emotion();

    public Map<String, Double> getWeigh(String word) throws IOException,
            ClassNotFoundException {
        Map<String, Double> res = new HashMap<String, Double>();
        String emotion = wordnetOriginWord.getCat(word);
        if (emotion != null) {
            res.put(emotion, WORDNET_WEIGH);
            return res;
        }
        emotion = wordnetWord.getCat(word);
        if (emotion != null && !emotion.contentEquals(OVERLAP_VALUE)) {
            res.put(emotion, SYNONMY_COEFF);
            return res;
        }

        /**
         * When build train data, use this part.
         */
        /*
        EmotionReliableWord w = trainedWord.get(word);
        if (w != null) {
            return w.getWeigh();
        }
        */

        res.put(NULL_EMOTION_CAT, 1.0);
        return res;
    }

    public Map<String, Double> computeReliability(String word)
            throws IOException, ClassNotFoundException {
        Map<String, Double> reliability = new HashMap<String, Double>();
        for (String emotion : emotionCategory.getEmotions()) {
            reliability.put(emotion, 0.0);
        }
        reliability.put(NULL_EMOTION_CAT, 0.0);
        Double total = 0.0;

        Map<String, Integer> relativeWords = relativityComputer
                .getRelativeWords(word);
        System.out.print(relativeWords + "\n");
        Iterator<Entry<String, Integer>> it = relativeWords.entrySet()
                .iterator();
        while (it.hasNext()) {
            Map.Entry<String, Integer> entry = (Map.Entry<String, Integer>) it
                    .next();

            Map<String, Double> weigh = getWeigh(entry.getKey());
            System.out.print(weigh + "\n");
            Iterator<Entry<String, Double>> weighIt = weigh.entrySet()
                    .iterator();
            while (weighIt.hasNext()) {
                Map.Entry<String, Double> weighEntry = (Map.Entry<String, Double>) weighIt.next();
                double weighTemp = reliability.get(weighEntry.getKey());
                // TODO: Multiply frequency. entry.getValue();
                weighTemp += weighEntry.getValue();
                total += weighEntry.getValue();
                reliability.put(weighEntry.getKey(), weighTemp);
            }

        }

        for (String emotion : emotionCategory.getEmotions()) {
            double tmp = reliability.get(emotion);
            reliability.put(emotion, tmp / total);
        }
        double tmp = reliability.get(NULL_EMOTION_CAT);
        reliability.put(NULL_EMOTION_CAT, tmp / total);
        // System.out.print(reliability + "\n" + total);
        return reliability;
    }

    public void train(String word) throws IOException, ClassNotFoundException {
        System.out.print(word + "\n");
        Map<String, Double> weigh = computeReliability(word);
        if (weigh.get(NULL_EMOTION_CAT) < 0.75) {
            trainedWord.insert(new EmotionReliableWord(word, weigh));
            System.out.print("word is : " + word + weigh + "\n");
        }
    }

    public void train() throws IOException, ClassNotFoundException {
        // trainedWord.clear();
        /**
         * Train words getted from RedisWords.
         */
        RedisWords redisWords = new RedisWords();
        Set<String> words = redisWords.getAll();

        for (String word : words) {
            if (wordnetOriginWord.getCat(word) == null
                    && wordnetWord.getCat(word) == null
                    && trainedWord.get(word) == null) {
                train(word);
            }
        }

        /**
         * Train words getted from comments.
         */
        /*
         * RedisComments redisComments = new RedisComments(); Category cat = new
         * Category(); List<Comment> trainingComments = redisComments
         * .getAll(cat.getCategory());
         * 
         * for (Comment comment : trainingComments) { for (String word :
         * comment.getWords()) { train(word); } }
         */
    }

    public static double getSynonmyCoeff() {
        return SYNONMY_COEFF;
    }

    public static double getTrainedCoff() {
        return TRAINED_COFF;
    }
}
