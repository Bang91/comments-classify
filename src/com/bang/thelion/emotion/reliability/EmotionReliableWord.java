package com.bang.thelion.emotion.reliability;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class EmotionReliableWord  implements Serializable{
    private static final long serialVersionUID = 1L;
    
    private String word;
    private Map<String, Double> weigh = new HashMap<String, Double>();
    private String type;
    
    public EmotionReliableWord(String word) {
        this.word = word;
    }
    public EmotionReliableWord(String word, Map<String, Double> weigh) {
        this.word = word;
        this.weigh = weigh;
    }
    public EmotionReliableWord(String word, String emotion, double weigh) {
        this.word = word;
        addWeigh(emotion, weigh);
    }
    
    public EmotionReliableWord(String word, String type) {
        this.word = word;
        this.type = type;
    }
    
    public void addWeigh(String emotion, double weigh) {
        this.weigh.put(emotion, weigh);
    }
    
    public String getWord() {
        return word;
    }
    
    public String getType() {
        return type;
    }

    public Map<String, Double> getWeigh() {
        return weigh;
    }

    public void setWeigh(Map<String, Double> weigh) {
        this.weigh = weigh;
    }
}
