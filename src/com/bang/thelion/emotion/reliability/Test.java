package com.bang.thelion.emotion.reliability;

import java.io.IOException;

import com.bang.thelion.comment.vectorization.RedisIGWords;
import com.bang.thelion.emotion.RedisEmotionWordsManager;

public class Test {
    public static void main(String args[]) throws IOException,
            ClassNotFoundException {
        /**
         * Test for class RedisEmotionReliableWord.
         */
        /*
         * RedisEmotionReliableWord wordManager = new
         * RedisEmotionReliableWord(); wordManager.insert(new
         * EmotionReliableWord("happy", "joy", 0.9)); // wordManager.clear();
         * EmotionReliableWord word = wordManager.get("happy");
         * System.out.print(word.getWeigh()); System.out.print(word.getWord());
         */

        EmotionReliableWordTrainer trainer = new EmotionReliableWordTrainer();
        // trainer.train("2");
        trainer.train();

        /**
         * Compute the IG value of the emotion words.
         */
        // TODO: Running this part.
        /*
        RedisIGWords igWordsManager = new RedisIGWords("igwords");
        RedisIGWords emotionIGWordsManager = new RedisIGWords("emotionIgWords");

        RedisEmotionReliableWord trainedWord = new RedisEmotionReliableWord();
        RedisEmotionWordsManager wordnetWord = new RedisEmotionWordsManager(
                "wordnet");
        System.out.print(wordnetWord.getAll());
        emotionIGWordsManager.clear();
        for (String word : wordnetWord.getAll()) {
            double tmp = igWordsManager.get(word);
            if (tmp != -1)
                emotionIGWordsManager.add(tmp, word);
            if (tmp > 0)System.out.print("\n" + word + " : " + tmp + " OK");
        }
        for (String word : trainedWord.getAllWords()) {
            emotionIGWordsManager.add(igWordsManager.get(word), word);
        }
        */
    }
 
}
