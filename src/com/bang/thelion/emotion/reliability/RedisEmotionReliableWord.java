package com.bang.thelion.emotion.reliability;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.codec.binary.Base64;

import redis.clients.jedis.Jedis;

public class RedisEmotionReliableWord {
    private final String REDIS_KEY = "reliableWord";
    private static final String HOST = "localhost";
    private static final int PORT = 6379;
    private final Jedis jedis = new Jedis(HOST, PORT);
    
    public static String transformToString(EmotionReliableWord m) throws IOException {
        final ByteArrayOutputStream byteArrayStream = new ByteArrayOutputStream();
        ObjectOutputStream objectStream = new ObjectOutputStream(
                byteArrayStream);
        objectStream.writeObject(m);
        objectStream.close();
        return new Base64().encodeToString(byteArrayStream.toByteArray());
    }

    public static EmotionReliableWord transformToWord(final String value)
            throws IOException, ClassNotFoundException {
        byte[] data = new Base64().decode(value);
        ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(
                data));
        EmotionReliableWord o = (EmotionReliableWord) ois.readObject();
        ois.close();
        return o;
    }
    
    public void insert(EmotionReliableWord w) throws IOException {
        jedis.hset(REDIS_KEY, w.getWord(), transformToString(w));
    }
    
    public EmotionReliableWord get(String word) throws IOException, ClassNotFoundException {
        String value = jedis.hget(REDIS_KEY, word);
        if (value != null) return transformToWord(value);
        else return null;
    }
    
    public Map<String, Double> getWeigh(String word) throws IOException, ClassNotFoundException {
        EmotionReliableWord w = get(word);
        if (w != null) {
            return w.getWeigh();
        }
        return null;
    }
    
    public Set<String> getAllWords() {
            Map<String, String> map = jedis.hgetAll(REDIS_KEY);
            return map.keySet();
    }
    
    public void remove(String word) {
        jedis.hdel(REDIS_KEY, word);
    }
    
    public void clear() {
        // TODO:
        Set<String> fields = jedis.hkeys(REDIS_KEY);
        Iterator<String> field = fields.iterator();
        for (; field.hasNext();) {
            remove(field.next());
        }
    }
}
