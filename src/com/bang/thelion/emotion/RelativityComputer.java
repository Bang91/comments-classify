package com.bang.thelion.emotion;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;

import com.bang.thelion.comment.lemma.Category;
import com.bang.thelion.comment.lemma.Comment;
import com.bang.thelion.comment.lemma.RedisComments;
import com.bang.thelion.emotion.similarity.CosineSimilarity;

public class RelativityComputer {
    private static final int DEFAULT_RELATIVITY_WINDOW_LEN = 3;

    private int relativityWindowLen;
    private List<Comment> comments;
    private RedisEmotionWordsManager emotionWordsManager;
    private Emotion emotion = new Emotion();
    private RedisTrainedWord trainedWordManager = new RedisTrainedWord();
    private List<String> emotions = emotion.getEmotions();

    public RelativityComputer() {
        this(DEFAULT_RELATIVITY_WINDOW_LEN);
    }

    public RelativityComputer(int relativityWindowLen) {
        this.emotionWordsManager = new RedisEmotionWordsManager();
        this.relativityWindowLen = relativityWindowLen;

        RedisComments redisComments = new RedisComments();
        Category cat = new Category();
        comments = redisComments.getAll(cat.getCategory());
    }
    
    /**
     * Compute emotion label in class EmotionClassificationTrainer.
     */
   
    public String emotionLabel(String word) {
        Map<String, Double> similarity = computeSimilarity(word);
        Iterator<Entry<String, Double>> it = similarity.entrySet().iterator();
        double max = 0;
        String res = null;
        while (it.hasNext()) {
            Map.Entry<String, Double> entry = (Map.Entry<String, Double>) it.next();
            if (entry.getKey() != null && entry.getValue() > max) {
                res = entry.getKey();
                max = entry.getValue();
            }
        }
        String emot = emotionWordsManager.getCat(res);
        if (emot != null) return emot;
        else return trainedWordManager.getEmotion(res);
    }
    
    public Map<String, Double> computeSimilarity(String word) {
        CosineSimilarity cosinSimilarity = new CosineSimilarity();
        Map<String, Double> res = new HashMap<String, Double>();
        List<String> relativeWords = emotionWordsManager.getAll();
        
        List<Double> vectorB = computeEmotionVector(word);
        for (String relativeWord : relativeWords) {
            List<Double> vectorA = computeEmotionVector(relativeWord);
            double similarity = cosinSimilarity.cosine(vectorA, vectorB, emotions.size());
            if (similarity > 0) {
                System.out.print(vectorA + " -- vectorA\n");
                System.out.print(vectorB + " -- vectorB\n");
                System.out.print(relativeWord + ":" + similarity + " -- similarity\n");
                res.put(relativeWord, similarity);
            }
        }
        
        /*
        Collections.sort(res, new Comparator<Map.Entry<String, Double>>() {   
            @Override
            public int compare(Map.Entry<String, Double> o1, Map.Entry<String, Double> o2) {      
                return (o2.getValue() - o1.getValue()) > 0 ? 1 : -1; 
                //return (o1.getKey()).toString().compareTo(o2.getKey());
            }
        }); 
        */
        return res;
    }

    public List<Double> computeEmotionVector(String word) {
        List<Double> res = new ArrayList<Double>();
        Map<String, Integer> emotionsValue = getRelativeWords(word);

        for (int i = 0; i < emotions.size(); i++) {
            int total = emotionWordsManager.getFrequency(emotions.get(i))
                    + trainedWordManager.getFrequency(emotions.get(i));
            if (emotionsValue.containsKey(emotions.get(i))) {
                res.add(emotionsValue.get(emotions.get(i)) * 1.0 / (total + 1.0));
            } else res.add(0.0);
        }
        return res;
    }
    
    public Map<String, Integer> getRelativeWords(String word) {
        Map<String, Integer> res = new HashMap<String, Integer>();

        for (Comment comment : comments) {
            List<Integer> positions = appearPositions(word, comment.getWords());

            for (int pos : positions) {
                int start = (pos - relativityWindowLen) > 0 ? (pos - relativityWindowLen)
                        : 0;
                int end = (pos + relativityWindowLen) < comment.getWords()
                        .size() ? (pos + relativityWindowLen) : comment
                        .getWords().size();

                for (int i = start; i < end; i++) {
                    int count = 0;
                    String keyWord = comment.getWords().get(i);
                    // String emotion = emotionWordsManager.getCat(keyWord);
                    if (res.containsKey(keyWord))
                        count = res.get(keyWord);
                    count++;
                    res.put(keyWord, count);
                }
            }
        }
        return res;
    }

    public List<Integer> appearPositions(String word, List<String> words) {
        List<Integer> positions = new ArrayList<Integer>();
        for (int i = 0; i < words.size(); i++) {
            if (word.endsWith(words.get(i)))
                positions.add(i);
        }
        return positions;
    }
}
