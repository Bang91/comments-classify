package com.bang.thelion.emotion;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.bang.thelion.comment.lemma.Comment;
import com.bang.thelion.comment.lemma.GetComment;

import redis.clients.jedis.Jedis;

public class RedisEmotionWordsManager {
    private static final String DEFAULT_REDIS_HOST = "localhost";
    private static final int DEFAULT_REDIS_PORT = 6379;
    private String REDIS_WORDNET_KEY = "wordnetOrigin";
    // private static final String REDIS_EMOTION_FREQUENCY_KEY =
    // "emotionfrequency";

    private final Jedis jedis;
    private final String host;
    private final int port;

    private Map<String, Integer> frequency = new HashMap<String, Integer>();

    public RedisEmotionWordsManager() {
        this(DEFAULT_REDIS_HOST, DEFAULT_REDIS_PORT);
    }
    
    public RedisEmotionWordsManager(String redisKey) {
        this(DEFAULT_REDIS_HOST, DEFAULT_REDIS_PORT);
        this.REDIS_WORDNET_KEY = redisKey;
    }
    
    public RedisEmotionWordsManager(String host, int port) {
        this.host = host;
        this.port = port;
        this.jedis = new Jedis(host, port);
    }

    public void readWords(String path, String category) throws IOException,
            ClassNotFoundException, SQLException {
        GetComment getComment = new GetComment("comments");

        FileInputStream fin = new FileInputStream(path);
        BufferedReader br = new BufferedReader(new InputStreamReader(fin));
        String line;
        while ((line = br.readLine()) != null) {
            String[] words = line.split(" ");

            for (int i = 0; i < words.length; i++) {
                add(getComment.lemma(words[i]), category);
            }
        }

        br.close();
    }

    public void readFromEkmanWords(String path) throws IOException,
            ClassNotFoundException, SQLException {
        GetComment getComment = new GetComment("comments");

        FileInputStream fin = new FileInputStream(path);
        BufferedReader br = new BufferedReader(new InputStreamReader(fin));
        String line;
        while ((line = br.readLine()) != null) {
            String[] words = line.split(" ");
            System.out.print(words[1] + "\n");
            add(getComment.lemma(words[1]), words[0]);
        }

        br.close();
    }

    public void add(String word, String emotion) {
        // if (frequency.containsKey(emotion)) frequency.put(emotion,
        // frequency.get(emotion) + 1);
        // else frequency.put(emotion, 1);

        jedis.hset(REDIS_WORDNET_KEY, word, emotion);
    }

    public String getCat(String word) {
        if (jedis.hexists(REDIS_WORDNET_KEY, word))
            return jedis.hget(REDIS_WORDNET_KEY, word);
        return null;
    }

    public void computeFrequency() {
        Map<String, String> map = jedis.hgetAll(REDIS_WORDNET_KEY);
        Iterator<Entry<String, String>> it = map.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry<String, String> entry = (Map.Entry<String, String>) it
                    .next();
            if (frequency.containsKey(entry.getValue())) {
                frequency.put(entry.getValue(),
                        frequency.get(entry.getValue()) + 1);
            } else
                frequency.put(entry.getValue(), 1);
        }
    }

    public Map<String, String> getAllWords() {
        return jedis.hgetAll(REDIS_WORDNET_KEY);
    }

    public List<String> getAll() {
        final List<String> words = new ArrayList<String>();
        final Collection<String> wordsValues = jedis.hgetAll(REDIS_WORDNET_KEY)
                .keySet();
        for (final String wordValue : wordsValues) {
            words.add(wordValue);
        }
        return words;
    }

    public List<String> getByEmotion(String emotion) {
        List<String> res = new ArrayList<String>();
        Map<String, String> map = jedis.hgetAll(REDIS_WORDNET_KEY);
        Iterator<Entry<String, String>> it = map.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry<String, String> entry = (Map.Entry<String, String>) it
                    .next();
            if (entry.getValue().equalsIgnoreCase(emotion)
                    && entry.getKey().indexOf("http") < 0
                    && entry.getKey().indexOf("#") < 0) {

                res.add(entry.getKey());
            }
        }
        System.out.print(res);
        return res;
    }

    public int getFrequency(String emotion) {
        if (!frequency.containsKey(emotion))
            computeFrequency();
        if (frequency.containsKey(emotion))
            return frequency.get(emotion);
        return 0;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }
}
