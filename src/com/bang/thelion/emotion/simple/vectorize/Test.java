package com.bang.thelion.emotion.simple.vectorize;

import java.io.IOException;

public class Test {
    public static void main(String args[]) throws IOException, ClassNotFoundException {
        SimpleEmotionVectorTrainer trainer = new SimpleEmotionVectorTrainer();
        trainer.train();
    }
}
