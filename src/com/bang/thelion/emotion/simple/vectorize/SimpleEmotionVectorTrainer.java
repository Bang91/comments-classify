package com.bang.thelion.emotion.simple.vectorize;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.bang.thelion.comment.lemma.Category;
import com.bang.thelion.comment.lemma.Comment;
import com.bang.thelion.comment.lemma.RedisComments;
import com.bang.thelion.comment.vectorization.RedisIGWords;
import com.bang.thelion.comment.vectorization.TfIdf;
import com.bang.thelion.comment.weka.ArffFile;
import com.bang.thelion.emotion.Emotion;
import com.bang.thelion.emotion.RedisEmotionWordsManager;
import com.bang.thelion.emotion.reliability.EmotionReliableWord;
import com.bang.thelion.emotion.reliability.RedisEmotionReliableWord;

public class SimpleEmotionVectorTrainer {
    private static final double SYNONYMS_K = 0.5;
    private static final double RELIABLE_K = 0.07;

    private static final String NULL_EMOTION_CAT = "nullEmotion";

    private RedisEmotionWordsManager originWordManager = new RedisEmotionWordsManager(
            "wordnetOrigin");
    private RedisEmotionWordsManager wordnetManager = new RedisEmotionWordsManager(
            "wordnet");
    private RedisEmotionReliableWord reliableWordManager = new RedisEmotionReliableWord();

    // private RedisIGWords emotionIGWords = new RedisIGWords("emotionIgWords");
    private RedisIGWords emotionIGWords = new RedisIGWords("igwords");
    private Emotion emotionCategory = new Emotion();
    private TfIdf tfIdfComputer = new TfIdf();

    public SimpleEmotionVectorTrainer() {
        emotionCategory.addEmotion(NULL_EMOTION_CAT);
    }

    public Map<String, Double> computeEmotionWeigh(Comment comment)
            throws IOException, ClassNotFoundException {

        // System.out.print("Starting ......\n");
        Map<String, Double> res = new HashMap<String, Double>();
        for (String emotion : emotionCategory.getEmotions()) {
            res.put(emotion, 0.0);
        }
        List<String> words = comment.getWords();
        // List<Double> tfIdfWeigh = tfIdfComputer.computeVector(comment,
        // words);
        // System.out.print("finish tfIdf compute\n");
        for (int i = 0; i < words.size(); i++) {
            String emotion = originWordManager.getCat(words.get(i));
            // System.out.print("Finish Search in originWordManager\n");
            // if (emotionIGWords.get(words.get(i)) <= 0) continue;
            if (emotion != null) {
                // System.out.print("WordNet Origin : " + emotion + ":" + words.get(i) + "\n");
                double weighTemp = res.get(emotion);
                weighTemp += emotionIGWords.get(words.get(i)); // tfIdfWeigh.get(i);
                res.put(emotion, weighTemp);
            } else {

                emotion = wordnetManager.getCat(words.get(i));
                // System.out.print("Finish Search in wordnetManager\n");
                if (emotion != null) {
                    //System.out.print("WordNet Synonmy : " + emotion + ":"+ words.get(i) + "\n");
                    double weighTemp = res.get(emotion);
                    weighTemp += emotionIGWords.get(words.get(i)) * SYNONYMS_K; // tfIdfWeigh.get(i)
                                                                                // *
                                                                                // SYNONYMS_K;
                    res.put(emotion, weighTemp);
                } else {
                    
                    EmotionReliableWord reliableWord = reliableWordManager
                            .get(words.get(i));
                    if (reliableWord != null) {
                        Iterator<Entry<String, Double>> weighIt = reliableWord
                                .getWeigh().entrySet().iterator();

                        while (weighIt.hasNext()) {
                            Map.Entry<String, Double> weighEntry = (Map.Entry<String, Double>) weighIt
                                    .next();
                            double weighTemp = res.get(weighEntry.getKey());
                            res.put(weighEntry.getKey(), weighTemp
                                    + emotionIGWords.get(words.get(i))
                                    * weighEntry.getValue() * RELIABLE_K);
                        }
                    } else {
                    

                        double weighTemp = res.get(NULL_EMOTION_CAT);
                        weighTemp += emotionIGWords.get(words.get(i)); // tfIdfWeigh.get(i);
                        res.put(NULL_EMOTION_CAT, weighTemp);
                    }
                }
            }
        }

        // System.out.print("\nEmotion Map is : "+ res + "\n");
        return res;
    }

    public Map<String, Double> computeWeigh(Comment comment)
            throws IOException, ClassNotFoundException {
        Map<String, Double> weigh = computeEmotionWeigh(comment);
        // System.out.print("Finish Compute Emotion Weigh" + weigh + "\n");

        DecimalFormat formatter = new DecimalFormat("#.######");
        double total = 0.0;

        Iterator<Entry<String, Double>> weighIt = weigh.entrySet().iterator();
        while (weighIt.hasNext()) {
            Map.Entry<String, Double> weighEntry = (Map.Entry<String, Double>) weighIt
                    .next();
            total += weighEntry.getValue();
        }
        if (total == 0)
            total = 1;
        total = Double.parseDouble(formatter.format(total));
        weighIt = weigh.entrySet().iterator();
        while (weighIt.hasNext()) {
            Map.Entry<String, Double> weighEntry = (Map.Entry<String, Double>) weighIt
                    .next();
            if (weighEntry.getValue() != 0) {
                double weighTemp = Double.parseDouble(formatter
                        .format(weighEntry.getValue()));
                System.out.print(total + " : " + weighTemp + "\n");
                weighTemp = weighTemp / total;
                weighEntry.setValue(weighTemp);
            }
        }
        // System.out.print("Finish " + weigh + "\n");
        return weigh;
    }

    public List<Double> merge(List<Double> res, int a, int b) {
        double tmp = res.get(a);
        res.set(b, tmp + res.get(b));
        res.remove(a);
        return res;
    }

    public List<Double> train(Comment comment) throws IOException,
            ClassNotFoundException {
        // System.out.print("ok\n");
        Map<String, Double> weigh = computeWeigh(comment);
        // System.out.print(weigh + "\n");
        List<Double> res = new ArrayList<Double>();

        // System.out.print(emotionCategory.getEmotions().size() + "\n");
        for (String emotion : emotionCategory.getEmotions()) {
            if (emotion != NULL_EMOTION_CAT)
                res.add(weigh.get(emotion));
        }

        // System.out.print("Res is : " + res + "\n");
        /*
         * TODO: Merge emotion tag.
         */

        // res = merge(res, 2, 1);
        // res = merge(res, 4, 1);
        // res = merge(res, 3, 1);
        // res = merge(res, 1, 0);

        System.out.print("After Merge Res is : " + res + "\n");
        return res;
    }

    public void train() throws IOException, ClassNotFoundException {
        RedisComments redisComments = new RedisComments();
        Category category = new Category();

        ArffFile file = new ArffFile("TestVector");
        RandomAccessFile frecord = new RandomAccessFile(
                "RedisCommentMAPtoTrainedDocs", "rw");
        int i = 1;
        // for (String cat : category.getCategory()) {
            String cat = "notagcomments";
            List<Comment> trainingComments = redisComments
                    .getAllByCategory(cat);

            if (cat.equalsIgnoreCase("strongbuy"))
                cat = "buy";
            if (cat.equalsIgnoreCase("Short")
                    || cat.equalsIgnoreCase("StrongSell")
                    || cat.equalsIgnoreCase("scalp"))
                cat = "sell";

            for (Comment comment : trainingComments) {
                System.out.print("Trainning comment ......\n");
                frecord.writeBytes(i++ + " -- " + comment.getSentiment() + " "
                        + comment.getCompany() + ":" + comment.getMsg() + "\n");
                List<Double> weigh = train(comment);
                file.addData(cat, weigh);
            }
        // }
    }
}
