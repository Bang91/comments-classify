package com.bang.thelion.emotion;

import java.util.ArrayList;
import java.util.List;

public class Emotion {
    private final List<String> category = new ArrayList<String>();
    
    public Emotion() {
        category.add("anger");
        category.add("disgust");
        category.add("fear");
        category.add("joy");
        category.add("sadness");
        category.add("surprise");
    }
    
    public List<String> getEmotions() {
        return category;
    }
    
    public void addEmotion(String emotion) {
        category.add(emotion);
    }
    
    public String getByIndex(int index) {
        return category.get(index);
    }
}
