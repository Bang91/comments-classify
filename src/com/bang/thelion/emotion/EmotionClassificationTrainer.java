package com.bang.thelion.emotion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.bang.thelion.comment.lemma.Category;
import com.bang.thelion.comment.lemma.Comment;
import com.bang.thelion.comment.lemma.RedisComments;

public class EmotionClassificationTrainer {
    private static final int DEFAULT_RELATIVITY_WINDOW_LEN = 3;
    
    private List<Comment> trainingComments;
    private RedisEmotionWordsManager emotionWordsManager;
    private RedisTrainedWord trainedWord;
    private RelativityComputer relativityComputer = new RelativityComputer();
    
    public EmotionClassificationTrainer() {
        RedisComments redisComments = new RedisComments();
        Category cat = new Category();
        trainingComments = redisComments.getAll(cat.getCategory());

        this.emotionWordsManager = new RedisEmotionWordsManager();
        this.trainedWord = new RedisTrainedWord();
    }

    
    
    public void train() {
        for (Comment comment : trainingComments) {
            System.out.print(comment.getWords() + "\n");
            train(comment);
        }
    }

    public void train(Comment comment) {
        if (comment.getWords().size() <= 0)
            return;
        for (String word : comment.getWords()) {
            if (emotionWordsManager.getCat(word) == null && trainedWord.getEmotion(word) == null) {
                // String emotion = emotionLabel(word);
                String emotion = relativityComputer.emotionLabel(word);
                System.out.print(word + " : " + emotion + "\n");
                trainedWord.add(word, emotion);
            }
        }
    }
    
    /*
    public String emotionLabel(String word) {
        Map<String, Double> similarities = relativityComputer.computeSimilarity(word);
        // TODO: sort HashMap by value(Double).
    }
    */
    
    /**
     * The emotion kind is the emotion of max value's word. 
     */
   
    public String emotionLabel(String word) {
        Map<String, Integer> emotions = getRelativeWords(word);
        System.out.print(emotions + "\n");
        // TODO: 计算六类 emotion 词汇的总数，用 出现次数 / 总数 的值做比较，得到 max 。
        Iterator<Entry<String, Integer>> it = emotions.entrySet().iterator();
        double max = 0;
        String res = null;
        while (it.hasNext()) {
            Map.Entry<String, Integer> entry = (Map.Entry<String, Integer>) it
                    .next();
            // double fre = entry.getValue() / emotionWordsManager.getFrequency(res);
            if (entry.getKey() != null && ((entry.getValue() * 1.0 ) / emotionWordsManager.getFrequency(entry.getKey())) > max) {

                res = entry.getKey();
                max = (entry.getValue() * 1.0 ) / emotionWordsManager.getFrequency(res);
            }
        }
        System.out.print(res + " if NULL is not counted \n");
        if (15 * emotions.get(res) < emotions.get(null)) res = null;
        return res;
    }
    
    public Map<String, Integer> getRelativeWords(String word) {
        Map<String, Integer> res = new HashMap<String, Integer>();

        for (Comment comment : trainingComments) {
            List<Integer> positions = appearPositions(word, comment.getWords());

            for (int pos : positions) {
                int start = (pos - DEFAULT_RELATIVITY_WINDOW_LEN) > 0 ? (pos - DEFAULT_RELATIVITY_WINDOW_LEN)
                        : 0;
                int end = (pos + DEFAULT_RELATIVITY_WINDOW_LEN) < comment
                        .getWords().size() ? (pos + DEFAULT_RELATIVITY_WINDOW_LEN)
                        : comment.getWords().size();

                for (int i = start; i < end; i++) {
                    int count = 0;
                    String keyWord = comment.getWords().get(i);
                    String emotion = emotionWordsManager.getCat(keyWord);

                    if (res.containsKey(emotion))
                        count = res.get(emotion);
                    count++;
                    res.put(emotion, count);
                }
            }
        }
        return res;
    }

    public List<Integer> appearPositions(String word, List<String> words) {
        List<Integer> positions = new ArrayList<Integer>();
        for (int i = 0; i < words.size(); i++) {
            if (word.endsWith(words.get(i)))
                positions.add(i);
        }
        return positions;
    }
}
