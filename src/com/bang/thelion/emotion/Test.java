package com.bang.thelion.emotion;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;

//import com.bang.thelion.comment.lemma.Comment;
//import com.bang.thelion.comment.lemma.RedisComments;

public class Test {

    public static void main(String[] args) throws IOException, ClassNotFoundException, SQLException {
        EmotionClassificationTrainer trainer = new EmotionClassificationTrainer();
        
        /**
         * Clear trained words in Redis.
         */
        /*
        RedisTrainedWord trainedWord = new RedisTrainedWord();
        trainedWord.clear();
        */
        // RedisComments redisComments = new RedisComments("buy");
        // Comment comment = redisComments.getByCompanyAndMsg("Facebook", "1000");
        // System.out.print(comment.getWords());
        
        /**
         * Train.
         */
        // trainer.train();
        
        /**
         * Get the total words count of each emotion category.
         */
        /*
        RedisTrainedWord trainedWord = new RedisTrainedWord();
        Emotion emotions = new Emotion();
        for (String emotion : emotions.getEmotions()) {
            System.out.print(trainedWord.getFrequency(emotion) + ":" + emotion + "\n");
        }
        */
        
        /*
        RedisEmotionWordsManager emotionWords = new RedisEmotionWordsManager();
        System.out.print(emotionWords.getFrequency("joy") + ":joy\n");
        System.out.print(emotionWords.getFrequency("anger") + ":anger\n");
        System.out.print(emotionWords.getFrequency("disgust") + ":disgust\n");
        System.out.print(emotionWords.getFrequency("fear") + ":fear\n");
        System.out.print(emotionWords.getFrequency("sadness") + ":sadness\n");
        System.out.print(emotionWords.getFrequency("surprise") + ":surprise\n");
        */
        
        
        
        /*
        RelativityComputer relativityComputer = new RelativityComputer();
        // Map<String, Integer> relativity = relativityComputer.getRelativeWords("disgusted");
        String emotionLabel = relativityComputer.emotionLabel("google");
        
        System.out.print(emotionLabel + "\n");
        RedisEmotionWordsManager emotionWords = new RedisEmotionWordsManager("localhost", 6379);
        System.out.print(emotionWords.getFrequency("surprise") + "\n");
        
        */
        
        
        /**
         * Put words of wordNet into Redis. 
         * Need to do this once.
         */
        /*
        RedisEmotionWordsManager emotionWords = new RedisEmotionWordsManager("localhost", 6379);
       
        GetComment getComment = new GetComment("comments");
        getComment.lemma("exciting");
        getComment.lemma("lovely");
        getComment.lemma("grated");
       
        
        
        emotionWords.readWords("inquireraugmented/WordNetAffectEmotionLists/anger.txt", "anger");
        emotionWords.readWords("inquireraugmented/WordNetAffectEmotionLists/disgust.txt", "disgust");
        emotionWords.readWords("inquireraugmented/WordNetAffectEmotionLists/fear.txt", "fear");
        emotionWords.readWords("inquireraugmented/WordNetAffectEmotionLists/joy.txt", "joy");
        emotionWords.readWords("inquireraugmented/WordNetAffectEmotionLists/sadness.txt", "sadness");
        emotionWords.readWords("inquireraugmented/WordNetAffectEmotionLists/surprise.txt", "surprise");
        
        emotionWords.readFromEkmanWords("inquireraugmented/WordNetAffectEmotionLists/EkmanWordslist.txt");             
        */
        
        /**
         * Get word list from Reids.
         */
        
        
        
        File file = new File("surprise.txt");
        BufferedWriter writer = new BufferedWriter(new FileWriter(file));
        RedisTrainedWord trainedWord = new RedisTrainedWord();
        RedisEmotionWordsManager emotionWord = new RedisEmotionWordsManager("sysnonymwords");
        Emotion emotions = new Emotion();

        writer.write(emotionWord.getByEmotion("surprise") + "\n");
        // writer.write(trainedWord.getByEmotion("anger") + "\n");
        writer.close();
        for (String emotion : emotions.getEmotions()) {
        }
        
    }
}
